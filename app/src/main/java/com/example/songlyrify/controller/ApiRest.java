package com.example.songlyrify.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class for managing rest apis (get/post)
 *
 * @author Valentin Ros Duart.
 */
public class ApiRest {

    private URL url;
    private HttpURLConnection http;

    private final String endpoint;
    private final String token;

    /**
     * Constructor for the given endpoint and token.
     * @param endpoint the endpoint url.
     * @param token the token (will be used with bearer)
     */
    public ApiRest(String endpoint, String token) {
        this.endpoint = endpoint;
        this.token = token;
    }

    /**
     * Sets the url for the petition.
     * @param requestUrl the query.
     */
    private void setUrl(String requestUrl) {
        try {
            url = new URL(endpoint + requestUrl);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Opens the http connection to the API.
     */
    private void connect() {
        try {
            http = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Closes the http connection to the API.
     */
    private void disconnect() {
        if (http != null) {
            http.disconnect();
        }
    }

    /**
     * Makes an http request using GET method for the given query to the API.
     * @param requestUrl the query.
     * @return the content returned by the api as a string in JSON format.
     */
    public String getRequest(String requestUrl) {
        String content = null;
        setUrl(requestUrl);
        connect();
        http.setRequestProperty("Accept", "application/json");
        http.setRequestProperty("Authorization", "Bearer " + token);
        try {
            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                content = sb.toString();
                reader.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            disconnect();
        }
        return content;
    }

    /*
    /**
     * Makes an http request using POST method for the given query to the API.
     * @param requestUrl the query.
     * @return the .
     */
    /*@NonNull
    public int postRequest(String requestUrl) {
        setUrl(requestUrl);
        connect();
        try {
            http.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            ex.printStackTrace();
        }
        http.setDoOutput(true);
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("Accept", "application/json");
        http.setRequestProperty("Authorization", "Bearer " + TOKEN);
        http.setRequestProperty("Content-Length", "0");
        disconnect();
        return "";
        return 0;
    }
    */

}
