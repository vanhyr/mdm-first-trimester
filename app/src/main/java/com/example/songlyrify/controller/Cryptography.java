package com.example.songlyrify.controller;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Pair;

import com.password4j.Argon2Function;
import com.password4j.BadParametersException;
import com.password4j.Hash;
import com.password4j.Password;
import com.password4j.types.Argon2;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;

/**
 * Class for managing cryptography in a secure way. Right now supports:
 * Encrypting/decrypting data using Android Key Store, AES encryption and Base64 encoding.
 * Hashing using random salt and pepper using Password4j crypto library and Argon2id hash.
 */
public class Cryptography {

    /**
     * Generates a secret key using the Android Key Store method, which is stored in another
     * hardware chip, and therefore more secure.
     * The generated key is built with AES encryption, GCM for block mode and no padding.
     * @return the SecretKey.
     */
    private static SecretKey generateSecretKeyAESGCM() {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
            keyGenerator.init(
                    new KeyGenParameterSpec.Builder(
                            "GCMKey",
                            KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT
                    )
                    .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .build()
            );
            return keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException ex) {
            throw new RuntimeException("Failed to create a secret symmetric key", ex);
        }
    }

    /**
     * Generates a secret key using the Android Key Store method, which is stored in another
     * hardware chip, and therefore more secure.
     * The generated key is built with AES encryption, CBC for block mode and PKCS7 padding.
     * @return the SecretKey.
     */
    private static SecretKey generateSecretKeyAESCBC() {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
            keyGenerator.init(
                    new KeyGenParameterSpec.Builder(
                            "CBCKey",
                            KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT
                    )
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build()
            );
            return keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException ex) {
            throw new RuntimeException("Failed to create a secret symmetric key", ex);
        }
    }

    /**
     * Retrieves the secret key from Android Key Store.
     * @return the SecretKey.
     */
    private static SecretKey getSecretKey(String alias) {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            return (SecretKey) keyStore.getKey(alias, null);
        } catch (KeyStoreException | CertificateException | IOException | NoSuchAlgorithmException
                | UnrecoverableKeyException ex) {
            throw new RuntimeException("Failed to retrieve the secret symmetric key", ex);
        }
    }

    /**
     * Encrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, GCM for block mode and no padding.
     * @param data the data you want to encrypt.
     * @return a Pair, first the ivBytes, second the encrypted data.
     */
    public static Pair<byte[], byte[]> encryptAESGCM(String data) {
        try {
            Cipher cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_GCM + "/"
                            + KeyProperties.ENCRYPTION_PADDING_NONE
            );
            cipher.init(Cipher.ENCRYPT_MODE, generateSecretKeyAESGCM());
            byte[] ivBytes = cipher.getIV();
            byte[] encryptedBytes = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
            return new Pair<>(ivBytes, encryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | BadPaddingException | IllegalBlockSizeException ex) {
            throw new RuntimeException("Failed to encrypt the data", ex);
        }
    }

    /**
     * Encrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, CBC for block mode and PKCS7 padding.
     * @param data the data you want to encrypt.
     * @return a Pair, first the ivBytes, second the encrypted data.
     */
    public static Pair<byte[], byte[]> encryptAESCBC(String data) {
        try {
            Cipher cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7
            );
            cipher.init(Cipher.ENCRYPT_MODE, generateSecretKeyAESCBC());
            byte[] ivBytes = cipher.getIV();
            byte[] encryptedBytes = cipher.doFinal(data.getBytes(StandardCharsets.UTF_8));
            return new Pair<>(ivBytes, encryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | BadPaddingException | IllegalBlockSizeException ex) {
            throw new RuntimeException("Failed to encrypt the data", ex);
        }
    }

    /**
     * Encrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, GCM for block mode and no padding.
     * Then encodes using Base64 and no padding.
     * @param data the data.
     * @return the encrypted and then encoded pair of strings (ivBytes, data)
     */
    public static Pair<String, String> encryptAndEncodeAESGCM(String data) {
        return encode(encryptAESGCM(data));
    }

    /**
     * Encrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, CBC for block mode and PKCS7 padding.
     * Then encodes using Base64 and no padding.
     * @param data the data.
     * @return the encrypted and then encoded pair of strings (ivBytes, data)
     */
    public static Pair<String, String> encryptAndEncodeAESCBC(String data) {
        return encode(encryptAESCBC(data));
    }

    /**
     * Encodes using Base64 and no padding.
     * @param encryptedData a pair of bytes (ivBytes, data)
     * @return the pair encoded as strings.
     */
    private static Pair<String, String> encode(Pair<byte[], byte[]> encryptedData) {
        return new Pair<>(Base64.encodeToString(encryptedData.first, Base64.DEFAULT),
                Base64.encodeToString(encryptedData.second, Base64.DEFAULT));
    }

    /**
     * Decrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, GCM for block mode and no padding.
     * @param ivBytes the ivBytes.
     * @param encryptedData the encrypted data you want to decrypt.
     * @return the decrypted data.
     */
    public static String decryptAESGCM(byte[] ivBytes, byte[] encryptedData) {
        try {
            Cipher cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_GCM + "/"
                            + KeyProperties.ENCRYPTION_PADDING_NONE
            );
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey("GCMKey"), new GCMParameterSpec(128, ivBytes));
            byte[] decryptedDataBytes = cipher.doFinal(encryptedData);
            return new String(decryptedDataBytes, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException | BadPaddingException
                | IllegalBlockSizeException ex) {
            throw new RuntimeException("Failed to decrypt the data", ex);
        }
    }

    /**
     * Decrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, CBC for block mode and PKCS7 padding.
     * @param ivBytes the ivBytes.
     * @param encryptedData the encrypted data you want to decrypt.
     * @return the decrypted data.
     */
    public static String decryptAESCBC(byte[] ivBytes, byte[] encryptedData) {
        try {
            Cipher cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7
            );
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey("CBCKey"), new IvParameterSpec(ivBytes));
            byte[] decryptedDataBytes = cipher.doFinal(encryptedData);
            return new String(decryptedDataBytes, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException | BadPaddingException
                | IllegalBlockSizeException ex) {
            throw new RuntimeException("Failed to decrypt the data", ex);
        }
    }

    /**
     * Decrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, GCM for block mode and no padding.
     * Then encodes using Base64 and no padding.
     * @param ivBytes the ivBytes.
     * @param data the data.
     * @return the encrypted and then encoded pair of strings (ivBytes, data)
     */
    public static String decodeAndDecryptAESGCM(String ivBytes, String data) {
        Pair<byte[], byte[]> decodedPair = decode(ivBytes, data);
        return decryptAESGCM(decodedPair.first, decodedPair.second);
    }

    /**
     * Decrypts data using the secret key from Android Key Store and Cipher using
     * AES encryption, CBC for block mode and PKCS7 padding.
     * Then encodes using Base64 and no padding.
     * @param ivBytes the ivBytes.
     * @param data the data.
     * @return the encrypted and then encoded pair of strings (ivBytes, data)
     */
    public static String decodeAndDecryptAESCBC(String ivBytes, String data) {
        Pair<byte[], byte[]> decodedPair = decode(ivBytes, data);
        return decryptAESCBC(decodedPair.first, decodedPair.second);
    }

    /**
     * Decodes using Base64 and no padding.
     * @param encodedIvBytes the encoded ivBytes.
     * @param encodedData the encoded data.
     * @return the pair decoded as byte arrays.
     */
    private static Pair<byte[], byte[]> decode(String encodedIvBytes, String encodedData) {
        return new Pair<>(Base64.decode(encodedIvBytes, Base64.DEFAULT),
                Base64.decode(encodedData, Base64.DEFAULT));
    }

    /**
     * Creates an Argon2 function with 15MiB memory, 4 iterations, 2 paralellism and Argon2Id
     * algorithm.
     * @return the Argon2 function.
     */
    private static Argon2Function customArgon2Id() {
        return Argon2Function.getInstance(15360, 4, 2, 32, Argon2.ID, 19);
    }

    /**
     * Hashes data (passwords usually) using Argon2Id via the Password4j cryptographic library.
     * @param data the data.
     * @return the hashed data.
     */
    public static Hash hashArgon2Id(String data) {
        try {
            return Password.hash(data).addRandomSalt().addPepper("Argon2IdPepper").with(customArgon2Id());
        } catch (BadParametersException ex) {
            throw new RuntimeException("Failed to hash", ex);
        }
    }

    /**
     * Checks the given data (passwords usually) against the hashed data (usually from a db)
     * using Argon2Id via the Password4j cryptographic library.
     * @param data the data.
     * @param salt the salt.
     * @param hashedData the hashed data.
     * @return true if both are the same, false if not.
     */
    public static boolean checkHashArgon2Id(String data, String salt, String hashedData) {
        try {
            return Password.check(data, hashedData).addSalt(salt).addPepper("Argon2IdPepper").with(customArgon2Id());
        } catch (BadParametersException ex) {
            throw new RuntimeException("Failed to check hash", ex);
        }
    }

}
