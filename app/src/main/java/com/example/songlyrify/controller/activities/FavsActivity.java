package com.example.songlyrify.controller.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.songlyrify.R;
import com.example.songlyrify.controller.adapter.FavsRecyclerAdapter;
import com.example.songlyrify.model.Song;
import com.example.songlyrify.util.PopUp;
import com.orm.SugarContext;

import java.util.List;

/**
 * Favs activity, for showing favourite songs.
 *
 * @author Valentin Ros Duart.
 */
public class FavsActivity extends AppCompatActivity {

    private RecyclerView rclrVwFavs;
    private FavsRecyclerAdapter favsRecyclerAdapter;
    private List<Song> favSongs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favs);

        // Set the title of the activity
        setTitle("Favourites");

        rclrVwFavs = findViewById(R.id.rclrVwFavs);

        // Start and create db connection
        SugarContext.init(this);
        // Get the SharedPreferences
        SharedPreferences spSession = getSharedPreferences("Session", MODE_PRIVATE);
        // Get the songs for the actual logged user identifier
        favSongs = Song.find(Song.class, "user_email = ?", spSession.getString("email", ""));
        // Close the db connection
        SugarContext.terminate();

        // Pass the song list to the recycler adapter
        favsRecyclerAdapter = new FavsRecyclerAdapter(favSongs);

        // Set the recycler view custom adapter we created
        rclrVwFavs.setAdapter(favsRecyclerAdapter);
        // Set the layout to linear (although grid could of been used for example)
        rclrVwFavs.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * Set a custom menu.
     * @param menu the default menu.
     * @return true.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fav_menu, menu);
        return true;
    }

    /**
     * Handle menu events.
     * @param item the menu item.
     * @return true.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        final int homeItemid = R.id.home_item;
        final int settingsItemId = R.id.settings_item;
        final int logoutItemId = R.id.logout_item;
        switch (itemId) {
            case homeItemid:
                finish();
                break;
            case settingsItemId:
                startActivity(new Intent(FavsActivity.this, SettingsActivity.class));
                break;
            case logoutItemId:
                // Get the shared session preferences
                SharedPreferences sharedPreferences = getSharedPreferences("Session", MODE_PRIVATE);
                // Creating an Editor object to edit (write to the file)
                SharedPreferences.Editor spEditor = sharedPreferences.edit();
                // Set email to empty and isLogged to false
                spEditor.putString("email", "");
                spEditor.putBoolean("isLogged", false);
                // Apply edits
                spEditor.apply();
                // Go to login
                startActivity(new Intent(FavsActivity.this, LoginActivity.class));
                PopUp.showShortToast(this, "Logged out");
                break;
        }
        return true;
    }

    /**
     * When an menu item is selected, delete it from the db and delete it from the list.
     * @param item the item select.
     * @return if worked or not.
     */
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        // The current adapter position
        int adapterPosition = item.getGroupId();
        switch (itemId) {
            // The delete from fav menu item
            case 121:
                // Create a new dialog builder
                AlertDialog.Builder builder = new AlertDialog.Builder(FavsActivity.this);
                // Set the text of the dialog
                builder.setMessage("Are you sure you want to delete this song from favourites?")
                        .setTitle("Delete song");
                // When yes is clicked
                builder.setPositiveButton("Yes", (dialogInterface, i) -> {
                    // Start and create db connection
                    SugarContext.init(FavsActivity.this);
                    // Save the song in the db (favourites)
                    favSongs.get(adapterPosition).delete();
                    // Close the db connection
                    SugarContext.terminate();
                    // Remove the list item on the current adapter position
                    favSongs.remove(adapterPosition);
                    // Tell the adapter that item was removed
                    favsRecyclerAdapter.notifyItemRemoved(adapterPosition);
                    PopUp.showShortToast(FavsActivity.this, "Deleted from favourites");
                });
                // When no is clicked
                builder.setNegativeButton("No", (dialogInterface, i) -> {
                    // Do nothing
                });
                // Create the dialog from builder and show it
                builder.create().show();
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return false;
    }
}