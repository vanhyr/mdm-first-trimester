package com.example.songlyrify.controller.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.songlyrify.R;

/**
 * Activity for redirecting to HomeActivity (search) or to LoginActivity.
 * Checks if the user is logged in and decides based on that.
 * @author Valentin Ros Duart.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the SharedPreferences
        SharedPreferences spSession = getSharedPreferences("Session", MODE_PRIVATE);
        // If user is already logged in, just go to home page, no need to login again
        if (spSession.getBoolean("isLogged", false)) {
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
        } else {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }
    }
}