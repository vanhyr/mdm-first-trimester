package com.example.songlyrify.controller.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.songlyrify.R;
import com.example.songlyrify.controller.Cryptography;
import com.example.songlyrify.model.User;
import com.example.songlyrify.util.PopUp;
import com.example.songlyrify.util.Validator;
import com.google.android.material.textfield.TextInputLayout;
import com.orm.SugarContext;
import com.password4j.Hash;

/**
 * Register activity, for managing users register using db (sugar)
 *
 * @author Valentin Ros Duart.
 */
public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout tilEmailRegister, tilRepeatPassRegister, tilPassRegister;
    private EditText edtEmailRegister, edtRepeatPassRegister, edtPassRegister;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Set the title of the activity
        setTitle("Register");

        // Find UI elements by id
        btnRegister = findViewById(R.id.btnRegister);

        tilEmailRegister = findViewById(R.id.tilEmailRegister);
        tilRepeatPassRegister = findViewById(R.id.tilRepeatPassRegister);
        tilPassRegister = findViewById(R.id.tilPassRegister);

        edtEmailRegister = findViewById(R.id.edtEmailRegister);
        edtRepeatPassRegister = findViewById(R.id.edtRepeatPassRegister);
        edtPassRegister = findViewById(R.id.edtPassRegister);

        // Add listeners
        btnRegister.setOnClickListener(register);
    }

    /**
     * OnClickListener arrow function that registers the user and send him to the home view.
     * Triggers when btnRegister is pressed. If it doesn't exists, stores the user in the db.
     */
    private final View.OnClickListener register = view -> {
        // Clear error messages
        tilRepeatPassRegister.setError(null);
        // Get input data
        String inputEmail = edtEmailRegister.getText().toString();
        String inputRepeatPass = edtRepeatPassRegister.getText().toString();
        String inputPass = edtPassRegister.getText().toString();
        // Check if input data is valid
        if (Validator.isValidEmail(inputEmail) && Validator.isValidPass(inputRepeatPass)
                && Validator.isValidPass(inputPass)) {
            // If pass and repeat pass are the same, register
            if (inputPass.equals(inputRepeatPass)) {
                // Start and create db connection
                SugarContext.init(this);
                // Check if email exists in the db
                if (User.find(User.class, "email = ?", inputEmail).size() > 0) {
                    // Create the intent to go back to login
                    Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                    // Add the toast msg to display and user to the intent
                    i.putExtra("msg", "User already exists, please log in.");
                    i.putExtra("email", inputEmail);
                    // Close db connection
                    SugarContext.terminate();
                    // Go to login screen
                    startActivity(i);
                // Email doesn't exists then register
                } else {
                    // Using an AsyncTask for hashing (might be time consuming)
                    HashPass hashPass = new HashPass();
                    // Create the hash for that input pass
                    Hash passHash = hashPass.doInBackground(inputPass);
                    // Create a user with the input email, salt and hash
                    User inputUser = new User(inputEmail, passHash.getSalt(), passHash.getResult());
                    // Save the user into the db (table User)
                    inputUser.save();
                    PopUp.showShortToastySuccess(this, "Successfully registered");
                    SugarContext.terminate();
                    // Storing data into SharedPreferences
                    SharedPreferences sharedPreferences = getSharedPreferences("Session", MODE_PRIVATE);
                    // Creating an Editor object to edit (write to the file)
                    SharedPreferences.Editor spEditor = sharedPreferences.edit();
                    // Storing the key and its value as the data fetched from edittext
                    spEditor.putString("email", inputEmail);
                    spEditor.putBoolean("isLogged", true);
                    spEditor.commit();
                    // Go to home page
                    startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
                }
                // Close db connection
                SugarContext.terminate();
                // Pass and repeat pass are different
                } else {
                    tilRepeatPassRegister.setError("Passes are not the same, try again");
                    tilRepeatPassRegister.requestFocus();
                }
        // Input data is not valid, show toast and focus the email field
        } else {
            PopUp.showShortToastyError(this, "Input data is wrong, please, consider checking values");
            tilEmailRegister.requestFocus();
        }
    };

    /**
     * Do the hashing in an AsyncTask because the hashing algorithm I used (Argon2Id) might be
     * time consuming.
     */
    private static class HashPass extends AsyncTask<String, Void, Hash> {

        HashPass() {
            super();
        }

        @Override
        protected Hash doInBackground(String... strings) {
            String inputPass = strings[0];
            return Cryptography.hashArgon2Id(inputPass);
        }

    }

}