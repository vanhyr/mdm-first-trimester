package com.example.songlyrify.controller.adapter;

import android.content.Intent;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.example.songlyrify.R;
import com.example.songlyrify.controller.activities.DetailActivity;
import com.example.songlyrify.model.Song;

import java.util.List;

/**
 * Class for defining a custom recycler view for the favs activity using an adapter.
 * @author Valentin Ros Duart.
 */
public class FavsRecyclerAdapter extends RecyclerView.Adapter<FavsRecyclerAdapter.RecyclerHolder> {

    private final List<Song> favSongs;

    public FavsRecyclerAdapter(List<Song> favSongs) {
        this.favSongs = favSongs;
    }

    /**
     * When the view holder it's created generates the list items structure from the layout given.
     * @param parent the parent.
     * @param viewType the view type.
     * @return the new recycler holder.
     */
    @NonNull
    @Override
    public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_song_item_list, parent, false);
        return new RecyclerHolder(view);
    }

    /**
     * Link the info from the recycler holder and each list item.
     * @param holder the recycler holder.
     * @param position the position of the list item.
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {
        Song song = favSongs.get(position);

        holder.txtVwSongTitle.setText(song.getTitle());
        holder.txtVwSongArtist.setText(song.getArtist());

        // Set Glide settings through the request manager
        holder.requestManager.load(song.getImageUrl())
                // When image isn't loaded yet
                .placeholder(holder.progressDrawable)
                // When the url can't be resolved
                .error(R.mipmap.app_launcher_logo)
                //.error(R.drawable.ic_error_outline_white_24dp)
                // The image view element from the layout
                .into(holder.imgVwSongImage);
    }

    /**
     * Get the size of the list.
     * @return the song list size.
     */
    @Override
    public int getItemCount() {
        return favSongs.size();
    }

    /**
     * Class for defining the contents for each list item from the model.
     * @author Valentin Ros Duart.
     */
    public class RecyclerHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener {

        private final ConstraintLayout constraintLytFavSongItem;

        private final ImageView imgVwSongImage;
        private final TextView txtVwSongTitle;
        private final TextView txtVwSongArtist;

        private final CircularProgressDrawable progressDrawable;

        private final RequestManager requestManager;

        public RecyclerHolder(@NonNull View itemView) {
            super(itemView);

            constraintLytFavSongItem = itemView.findViewById(R.id.constraintLytFavSongItem);

            imgVwSongImage = itemView.findViewById(R.id.imgVwSongImage);
            txtVwSongTitle = itemView.findViewById(R.id.txtVwSongTitle);
            txtVwSongArtist = itemView.findViewById(R.id.txtVwSongArtist);

            // Initialize the circular progress drawable
            progressDrawable = new CircularProgressDrawable(itemView.getContext());

            // Set the progress drawable settings
            progressDrawable.setStrokeWidth(10f);
            progressDrawable.setStyle(CircularProgressDrawable.LARGE);
            progressDrawable.setCenterRadius(30f);
            progressDrawable.start();

            // Set the view where glide needs to load the image
            requestManager = Glide.with(itemView);

            // Add a listener to every item in the recycler
            constraintLytFavSongItem.setOnCreateContextMenuListener(this);
            constraintLytFavSongItem.setOnClickListener(goToDetail);
        }

        /**
         * OnClickListener arrow function when a song item is clicked.
         */
        private final View.OnClickListener goToDetail = view -> {
            // Get the clicked position to get that song from the songs list
            Song song = favSongs.get(getAdapterPosition());
            // Create the intent to go back to login
            Intent i = new Intent(view.getContext(), DetailActivity.class);
            // Add the toast msg to display and user to the intent
            i.putExtra("song_id", Integer.toString(song.getIdx()));
            view.getContext().startActivity(i);
        };

        /**
         * Add a context menu to every item in the recycler.
         * @param contextMenu the context menu.
         * @param view the view.
         * @param contextMenuInfo the context menu info.
         */
        @Override
        public void onCreateContextMenu(@NonNull ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            //contextMenu.setHeaderTitle("Select an action");
            // Add a context menu item (selected item position, item id, order, display text)
            contextMenu.add(this.getAdapterPosition(), 121, 0, "Delete")
                    // Context menus don't support icons, may not work
                    .setIcon(android.R.drawable.ic_menu_delete)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }

    }

}