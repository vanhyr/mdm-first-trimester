package com.example.songlyrify.controller.adapter;

import static android.content.Context.MODE_PRIVATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.example.songlyrify.R;
import com.example.songlyrify.controller.activities.DetailActivity;
import com.example.songlyrify.model.Song;
import com.example.songlyrify.util.PopUp;
import com.orm.SugarContext;

import java.util.List;

/**
 * Class for defining a custom recycler view for the home activity using an adapter.
 * @author Valentin Ros Duart.
 */
public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.RecyclerHolder> {

    private final List<Song> songs;

    public HomeRecyclerAdapter(List<Song> songs) {
        this.songs = songs;
    }

    /**
     * When the view holder it's created generates the list items structure from the layout given.
     * @param parent the parent.
     * @param viewType the view type.
     * @return the new recycler holder.
     */
    @NonNull
    @Override
    public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_item_list, parent, false);
        return new RecyclerHolder(view);
    }

    /**
     * Link the info from the recycler holder and each list item.
     * @param holder the recycler holder.
     * @param position the position of the list item.
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {
        Song song = songs.get(position);

        holder.txtVwSongTitle.setText(song.getTitle());
        holder.txtVwSongArtist.setText(song.getArtist());

        // Set Glide settings through the request manager
        holder.requestManager.load(song.getImageUrl())
                // When image isn't loaded yet
                .placeholder(holder.progressDrawable)
                // When the url can't be resolved
                .error(R.mipmap.app_launcher_logo)
                //.error(R.drawable.ic_error_outline_white_24dp)
                // The image view element from the layout
                .into(holder.imgVwSongImage);

        // Check whether each song from search exist in the favSongs list and if it does, set the
        // star to filled
        /*if (holder.favSongs.contains(song)) {
            holder.imgBtnFav.setImageResource(R.drawable.filled_star_small);
        }*/

    }

    /**
     * Get the size of the list.
     * @return the song list size.
     */
    @Override
    public int getItemCount() {
        return songs.size();
    }

    /**
     * Class for defining the contents for each list item from the model.
     * @author Valentin Ros Duart.
     */
    public class RecyclerHolder extends RecyclerView.ViewHolder {

        private final ConstraintLayout constraintLytHomeSongItem;

        private final ImageView imgVwSongImage;
        private final TextView txtVwSongTitle;
        private final TextView txtVwSongArtist;

        private ImageButton imgBtnFav;

        private final CircularProgressDrawable progressDrawable;

        private final RequestManager requestManager;

        private final List<Song> favSongs;

        public RecyclerHolder(@NonNull View itemView) {
            super(itemView);

            constraintLytHomeSongItem = itemView.findViewById(R.id.constraintLytHomeSongItem);

            imgVwSongImage = itemView.findViewById(R.id.imgVwSongImage);
            txtVwSongTitle = itemView.findViewById(R.id.txtVwSongTitle);
            txtVwSongArtist = itemView.findViewById(R.id.txtVwSongArtist);
            imgBtnFav = itemView.findViewById(R.id.imgBtnFav);

            // Initialize the circular progress drawable
            progressDrawable = new CircularProgressDrawable(itemView.getContext());

            // Set the progress drawable settings
            progressDrawable.setStrokeWidth(10f);
            progressDrawable.setStyle(CircularProgressDrawable.LARGE);
            progressDrawable.setCenterRadius(30f);
            progressDrawable.start();

            // Set the view where glide needs to load the image
            requestManager = Glide.with(itemView);

            imgBtnFav.setOnClickListener(addFav);

            // Start and create db connection
            SugarContext.init(itemView.getContext());
            // Get all the favourite songs from the db
            favSongs = Song.listAll(Song.class);
            // Close the db connection
            SugarContext.terminate();
            constraintLytHomeSongItem.setOnClickListener(goToDetail);
        }

        /**
         * OnClickListener arrow function when a imgBtnFav is clicked.
         */
        private final View.OnClickListener addFav = view -> {
            // Get the clicked position to get that song from the songs list
            Song song = songs.get(getAdapterPosition());
            // Get from SharedPreferences
            SharedPreferences spSession = view.getContext().getSharedPreferences("Session", MODE_PRIVATE);
            // Set user identifier in the song to track favourites for each user
            song.setUserEmail(spSession.getString("email", ""));
            imgBtnFav.setImageResource(R.drawable.filled_star_small);
            // Start and create db connection
            SugarContext.init(view.getContext());
            // Save the song in the db (favourites)
            song.save();
            // Close the db connection
            SugarContext.terminate();
            PopUp.showShortToast(view.getContext(), "Added to favourites");
        };

        /**
         * OnClickListener arrow function when a song item is clicked.
         */
        private final View.OnClickListener goToDetail = view -> {
            // Get the clicked position to get that song from the songs list
            Song song = songs.get(getAdapterPosition());
            // Create the intent to go back to login
            Intent i = new Intent(view.getContext(), DetailActivity.class);
            // Add the toast msg to display and user to the intent
            i.putExtra("song_id", Integer.toString(song.getIdx()));
            view.getContext().startActivity(i);
        };

    }

}