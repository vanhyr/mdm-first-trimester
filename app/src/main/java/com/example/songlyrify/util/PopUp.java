package com.example.songlyrify.util;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

/**
 * Class for showing pop ups (toasts, dialogs, etc.)
 */
public class PopUp {

    /**
     * Show a long toast.
     * @param context the context.
     * @param message the message to display.
     */
    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    /**
     * Show a short toast.
     * @param context the context.
     * @param message the message to display.
     */
    public static void showShortToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Show a short info toasty.
     * @param context the context.
     * @param message the message to display.
     */
    public static void showShortToastyInfo(Context context, String message) {
        Toasty.info(context, message, Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Show a short success toasty.
     * @param context the context.
     * @param message the message to display.
     */
    public static void showShortToastySuccess(Context context, String message) {
        Toasty.success(context, message, Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Show a short warning toasty.
     * @param context the context.
     * @param message the message to display.
     */
    public static void showShortToastyWarning(Context context, String message) {
        Toasty.warning(context, message, Toast.LENGTH_SHORT, true).show();
    }

    /**
     * Show a short error toasty.
     * @param context the context.
     * @param message the message to display.
     */
    public static void showShortToastyError(Context context, String message) {
        Toasty.error(context, message, Toast.LENGTH_SHORT, true).show();
    }

}
